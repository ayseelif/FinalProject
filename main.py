#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tkinter
import threading
from tkinter.filedialog import askopenfilename   #ask file path when open file
import os

import imutils
import load as load
import playsound as playsound
from PIL import Image, ImageTk
import cv2
from gtts import gTTS
from imutils.perspective import four_point_transform
from pytesseract import pytesseract
import tesserocr as tr
import shutil



name = ""
text_sound_frame = ()
img = ()
soundIsPlaying = False
soundStart = 0
soundStop = 0
slider = ()
contSound = 0
createSound = False
sound = ()
result = ""
count=0
sem=threading.Semaphore(1)
sem2=threading.Semaphore(1)
temp_col=False
k = 0
render = 0
text_converting = False
color_x = False
isStart = False
#t2 = ()
num_lines = -1
def create_gui():  # main menu  # boyutta değişiklik yapıldı
    ##############################################################
    global text_sound_frame
    global slider, soundStart, soundStop
    frame = tkinter.Frame(root)  # general frame
    frame.grid(row=0, column=0, padx=2, pady=2)

    photo_frame = tkinter.Frame(frame)  # photo frame
    photo_frame.grid(row=0, column=0, padx=2, pady=2)

    photo_old = tkinter.Label(photo_frame, width=60, height=20, bg="black")  # before any function
    photo_old.grid(row=0, column=0, padx=2, pady=0)


    image_open = Image.open("C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/img/open.png")
    open_photo = ImageTk.PhotoImage(image_open)
    upload_photo = tkinter.Button(photo_frame, image=open_photo, border=0, command=button_open)  # open photo(old)
    upload_photo.image = open_photo
    upload_photo.grid(row=0, column=1)

    photo_new = tkinter.Label(photo_frame, width=60, height=20, bg="black")  # after function
    photo_new.grid(row=0, column=2, padx=2, pady=0)

    text_sound_frame = tkinter.Frame(frame)  # frame of text and sound
    text_sound_frame.grid(row=1, column=0, padx=2, pady=2)

    text_frame()

    instruction = tkinter.Label(text_sound_frame, width=60, height=20, bg="white",
                                text="KULLANICI TALİMATLARI")  # after function
    instruction.grid(row=0, column=2, padx=50, pady=0)
    # sound_place = tkinter.Frame(text_sound_frame, width=60, height=10)  # sound frame (text speech)
    # sound_place.grid(row=0, column=1, padx=20, pady=2)
    #
    # image_play = Image.open("/home/ayse/PycharmProjects/finalProject/img/play.png")  # play button
    # play_photo = ImageTk.PhotoImage(image_play)
    # play = tkinter.Button(sound_place, image=play_photo, border=0, command=play_func)
    # play.image = play_photo
    # play.grid(row=0, column=0)
    #
    # image_stop = Image.open("/home/ayse/PycharmProjects/finalProject/img/pause.png")  # stop button
    # stop_photo = ImageTk.PhotoImage(image_stop)
    # stop = tkinter.Button(sound_place, image=stop_photo, text="Pause", border=0, command=stop_func)
    # stop.image = stop_photo
    # stop.grid(row=0, column=1, padx=2, pady=2)
    #
    # slider = tkinter.Scale(sound_place, from_=soundStart, to=soundStop, orient=tkinter.HORIZONTAL,
    #                        # slider to show voice
    #                        length=400, command=slider_func)
    # slider.grid(row=0, column=2)

    buttons = tkinter.Frame(frame, width=60, height=5)  # button frame
    buttons.grid(row=2, column=0, padx=5, pady=5)

    threshold = tkinter.Button(buttons, text="BAŞLA", border=0, bg="red", width=25, height=3,
                               command=button_threshold)  # apply threshold on old photo or new photo if it exists
    threshold.grid(row=0, column=0, padx=7, pady=5)

    stop = tkinter.Button(buttons, text="DUR", border=0, bg="red", width=25, height=3,
                          command=stop_function)  # apply flatting on old photo or new photo if it exists
    stop.grid(row=0, column=1, padx=7, pady=5)

    # page_flatting = tkinter.Button(buttons, text="SAYFA TRANSFORM", border=0, bg="red", width=25, height=5,
    #                                command=button_flatting)  # apply flatting on old photo or new photo if it exists
    # page_flatting.grid(row=0, column=1, padx=7, pady=5)
    #
    # clip_page = tkinter.Button(buttons, text="PERSPEKTİF", border=0, bg="red", width=25, height=5,
    #                            command=button_clip)  # apply clip page on old photo or new photo if it exists
    # clip_page.grid(row=0, column=2, padx=7, pady=5)
    #
    # all_func = tkinter.Button(buttons, text="HEPSİ", border=0, bg="red", width=25, height=5,
    #                           command=button_all)  # aplly all function on old photo
    # all_func.grid(row=0, column=3, padx=7, pady=5)
    #########################################################################################


def text_frame(): # boyutta değişiklik yapıldı
    global text_converting
    print("text_frame")
    yscrollbar = tkinter.Scrollbar(text_sound_frame, command=())
    yscrollbar.grid(sticky="NSW", row=0, column=1, rowspan=2)

    xscrollbar = tkinter.Scrollbar(text_sound_frame, orient=tkinter.HORIZONTAL, command=())
    xscrollbar.grid(row=1, column=0, rowspan=2, sticky="EW")

    var = tkinter.StringVar()

    if not text_converting:
        print("not text_converting")
        if name != "":
            if not text_converting:
                print("boş değil")
                convert_text()
        with open("C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/finalProject.txt", 'r', encoding='utf-8') as myfile:
            data = myfile.read()
        myfile.close()
    else:
        print("text_converting")
        with open("C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/temple.txt", 'r') as myfile:
            data = myfile.read()
        myfile.close()

    var.set("")
    var.set(data)

    text = tkinter.Text(text_sound_frame, wrap=tkinter.NONE, width=60, height=15, yscrollcommand=yscrollbar.set,
                        xscrollcommand=xscrollbar.set)

    text.insert(tkinter.END, var.get())
    text.grid(row=0, column=0, padx=2, pady=0)
    text.config(state=tkinter.DISABLED)  # not editable
    yscrollbar.config(command=text.yview)
    xscrollbar.config(command=text.xview)

def perspective_trans(image): ####yeni eklenen fonksiyon

    screenCnt = ''
    ratio = image.shape[0] / 500.0
    orig = image.copy()
    image = imutils.resize(image, height=500)

    blurImage = cv2.medianBlur(image, 15)
    edged = cv2.Canny(blurImage, 0, 20)
    #cv2.imshow("EDGE DETECTION", imutils.resize(edged, height=450))
    cnts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if imutils.is_cv2() else cnts[1]
    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:5]
    for c in cnts:
        peri = 0.1 * cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, peri, True)
        #print(len(approx))
        cv2.drawContours(image, [c], -1, (255, 255, 0), 4)
        if len(approx) == 4:
            screenCnt = approx
            #cv2.imshow("Outline", image)
            break

    #cv2.imshow("Outline", image)
    #cv2.waitKey(0)
########################################################################################
    if len(approx) != 4:
        return orig
    else:
        warped = four_point_transform(orig, screenCnt.reshape(4, 2) * ratio)
########################################################################################
    #cv2.waitKey(0)
    print(screenCnt.reshape(4, 2) * ratio)
    #cv2.imshow("Scanned", imutils.resize(warped, height=450))
    #cv2.imwrite("scanned.jpg", warped)

    return warped


def stream(label):  # boyutta değişiklik yapıldı
    global render
    global isStart
    blur = cv2.medianBlur(label, 5)
    th = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    cv2.imwrite("C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/img/temple.jpg", th)

    load = Image.open("C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/img/temple.jpg")
    load = load.resize((478, 305), Image.ANTIALIAS)
    render = ImageTk.PhotoImage(load)
    img = tkinter.Label(image=render)
    img.image = render
    img.place(x=558, y=2)
    if isStart:
        text_frame()

def convert_text():
    global result, count, text_converting, isStart
    print("text")
    img = Image.open("C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/img/temple.jpg")
    pytesseract.tesseract_cmd = 'C:/Program Files (x86)/Tesseract-OCR/tesseract.exe'
    result = pytesseract.image_to_string(img, lang='tur')

    with open("C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/finalProject.txt", mode='w', encoding='utf-8') as file:
        file.write(result + '\n')
        print(">Dosya dönüştürme bitti!")

    count = 0
    th2 = threading.Thread(target=readFile)
    if not th2.isAlive() and isStart:
        th2.start()
    text_converting = True
    file.close()


# noinspection PyGlobalUndefined
def button_threshold():#### perspektif ile değişen fonksiyon
    global img, text_converting, isStart

    if name == "":
        print("resim secilmedi")
    else:
        isStart = True
        load = Image.open(name.encode('utf-8'))
        blur_photo = cv2.imread(name, 0)
        ########yeni eklendi
        #####################################################
        blur_photo = perspective_trans(blur_photo)
        #####################################################
        thread = threading.Thread(target=stream, args=(blur_photo,))
        thread.daemon = 1
        thread.start()
        text_converting = False


def button_open(): # boyutta değişiklik yapıldı
    global name, img, isStart
    if "" != name:
        img.destroy()
    name = askopenfilename(initialdir="C:/Users/elif/Desktop/FINAL_PROJECT/",
                                        filetypes=[
                                            ("Image Files", ("*.jpg", "*.gif", "*.png")),
                                            ("JPG", '*.jpg'),
                                            ("JPEG",'*.jpeg'),
                                            ("GIF", '*.gif'),
                                            ("PNG", '*.png'),
                                            ('All', '*')
                                        ],
                                        title="Choose an image file."
                                        )
    print(name)
    isStart = False
    load = Image.open(name)
    load = load.resize((478, 315), Image.ANTIALIAS)
    render = ImageTk.PhotoImage(load)
    img = tkinter.Label(image=render)
    img.image = render
    img.place(x=5, y=2)


def button_flatting():
    print("flatting")


def button_clip():
    print("clip")


def button_all():
    print("all")


def play_func():
    global slider, contSound, soundIsPlaying, createSound
    print("play")
    if createSound == True:
        soundIsPlaying = True
        slider.set(contSound)


def stop_func():
    global soundIsPlaying, createSound
    print("stop")
    if createSound == True:
        soundIsPlaying = False


def threadFunc(label):
    global slider, contSound, soundStop, sound

    for x in sound:
        if createSound == True:
            print("thread"+str(contSound))
            slider.set(contSound)
        else:
            while True:
                print("sonsuzda")


def stop_function():
    global isStart
    isStart = False

def color(box):#takip fonksiyonu
        global k, temp_col, color_x, isStart
        color_x = True
        image = cv2.imread('C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/img/temple.jpg')
        while True:
            if temp_col and isStart:
                temp_col = False

                output = image.copy()
                overlay = image.copy()

                x = box[k][1]['x']
                y = box[k][1]['y']
                w = box[k][1]['w']
                h = box[k][1]['h']
                cv2.rectangle(overlay, (x, y), (x + w, y + h), (0, 255, 0), -1)
                cv2.addWeighted(overlay, 0.5, output, 0.5, 0, output)
                output = Image.fromarray(output)
                output = output.resize((483, 380), Image.ANTIALIAS)
                render1 = ImageTk.PhotoImage(output)
                img = tkinter.Label(image=render1)
                img.image = render1
                img.place(x=558, y=2)
                k = k + 1

                if not isStart:
                    color_x = False
                    k = 0
                    break


def color_word():#takip fonksiyonu

    img = cv2.imread('C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/img/temple.jpg')
    pil_img = Image.fromarray(img)
    api = tr.PyTessBaseAPI()
    api.SetImage(pil_img)
    boxes = api.GetComponentImages(tr.RIL.TEXTLINE, True)

    box = boxes
    th_c = threading.Thread(target=color, args=(box,))
    th_c.start()

    #api.End()

def slider_func(label):
    global contSound, soundIsPlaying, slider
    print("slider function " + str(soundIsPlaying))
    if soundIsPlaying == True and createSound == True:
        contSound +=1
    print(contSound)


def speech(line, ):
    global count, isStart
    print("thread başlangıç---" + str(count))

    tts = gTTS(text=line, lang='TR')
    print("count: " + str(count))
    if count == 1 and isStart:
        th2 = threading.Thread(target=speak)
        th2.start()

    tts.save("C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/sound" + str(count) + ".mp3")
    print(">sound file is updated !" + str(count))

    count += 1


def speak():####temp_col değişkeni takip için
    global count, temp_col, isStart, num_lines
    c = 0
    while True:
        if c < count:
            temp_col = True
            print("c--------------" + str(c))
            sem.acquire()
            name_speak = 'C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/sound' + str(c) + ".mp3"
            playsound.playsound(name_speak)
            sem.release()
            print("okuma bitti --------------- " + name_speak)
            c += 1
        if c == count and c == num_lines:
            isStart = False
            num_lines = -1
        if not isStart:
            break

def readFile():####içinde takip için yeni bir thread var
    global count, color_x, isStart, num_lines
    f = open("C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/finalProject.txt", 'r', encoding='utf-8')
    fwrite = open("C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/temple.txt", mode='w')
    i_read_file = 0
    line = ''
    while True:

        s = f.readline()
        if not s or not isStart:
            break
        if s.isspace():
            continue
        while i_read_file < s.__len__():
            if ord(s[i_read_file]) == 32 or (
                    not (not (48 <= ord(s[i_read_file]) <= 57) and not (65 <= ord(s[i_read_file]) <= 90) and not (
                            97 <= ord(s[i_read_file]) <= 122) and not (s[i_read_file] == 'İ') and not (
                            s[i_read_file] == 'Ö')) or (s[i_read_file] == 'Ü') or s[i_read_file] == 'ö' or s[
                        i_read_file] == 'ü' or s[i_read_file] == 'ş' or
                    s[i_read_file] == 'Ş' or s[i_read_file] == 'ı' or s[i_read_file] == 'ç' or s[i_read_file] == 'Ç' or
                    s[i_read_file] == 'ğ' or s[i_read_file] == 'Ğ' or s[
                        i_read_file] == 'â' or s[i_read_file] == '\n'):
                line = line + s[i_read_file]
            i_read_file = i_read_file + 1
        fwrite.write(line)
        # print(line)

        # takip için yeni thread
        if not color_x:
            th_color = threading.Thread(target=color_word)
            th_color.start()
            th_color.join()


        th = threading.Thread(target=speech, args=(line,))
        th.start()
        th.join()

        line = ''

        i_read_file = 0

    if count == 1:
            playsound.playsound("C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/sound0.mp3")
    print("readFile son----------")

    f.close()
    fwrite.close()
    text_frame()

    num_lines = len(open('C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/temple.txt').readlines())
    print("satır sayısı:" + str(num_lines))


"""def convert_sound():
    global sound, soundStop, result
    print("sound")

    tts = gTTS(text=result, lang='TR', slow=False) # thread eklersek buradaki hız sorununu çözeriz
    tts.save("C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/sound.mp3")
    sound = open("C:/Users/elif/Desktop/FINAL_PROJECT/Gitlab-project/sound.mp3", 'rb')
    print (sound.__sizeof__())
    soundStop = sound.__sizeof__()
    createSound = True
    print(">Sese dönüştürme bitti!")"""


root = tkinter.Tk()
root.wm_title("Kitaptan Sese Uygulaması-Demo")
root.geometry("1050x780")
create_gui()
root.mainloop()
