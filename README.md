# FinalProject

**Text to Speech with Python**

In this project for the visually impaired people, it is aimed to read any text.
The position of the camera is adjusted to the user by voice instructions. 
For Turkish texts, the real-time image is converted to sound.

The whole project is in **develop** branch.It can be run by installing python3, gtts, tesseract,
East model on the computer and installing the droidcam application on the phone.
More detailed information can be found in the project report.

The following braches are opened for testing purposes and do not require any merge operation.
1.  ayrılmış-hali
2.  location-of-characters
3.  text-to-speech

In **skew-of-image**  branch, the project is running in a single page document.